<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div id="index">
  <section class="hero is-dark is-bold is-large">
    <div class="hero-body">
      <div id="particle-js"></div>
      <div class="container has-text-centered">
        <!--<h1 class="title wow zoomIn" data-wow-duration="1s">
          <?= ($this->session->login == TRUE ? 'Hello ' . $this->session->name : 'Selamat Datang '); ?>
        </h1>-->
		<h1 class="title wow zoomIn" data-wow-duration="1s">
         <!-- <IMG SRC="http://3.bp.blogspot.com/-YR5d526KIfg/Tyvt_gO4CTI/AAAAAAAAEvo/R_U0JM0RviQ/s1600/LOGO+KABUPATEN+BLORA.png" width="300px">-->
		  <IMG SRC="<?= base_url('assets/images/logokabBlora1.png'); ?>" width="450px">
        </h1>
		<p><br>
		<p><br>
		<h1 class="title wow zoomIn" data-wow-duration="4s">
          PEMERINTAH KABUPATEN BLORA
		 </h1>
		<p><br>
		<p><br>
		<h1 class="title wow zoomIn" data-wow-duration="7s" >
			<font size="8">SURVAI DAN PEMETAAN KABUPATEN BLORA</font> 
        </h1>
        <!--<h2 class="subtitle wow zoomIn" data-wow-duration="1s" data-wow-delay="0.2s">
          <?= ($this->session->login == TRUE ? 'Welcome To - <i class="fas fa-map"></i> Search Map' : '<i class="fas fa-map"></i> Peta Kota Blora'); ?>
        </h2>-->
      </div>
    </div>
  </section>

  <section class="hero is-medium">
    <div class="hero-body">
      <div class="container">
        <div class="has-text-centered wow zoomIn" data-wow-duration="1s">
          <p class="title">
            Menu
          </p>
          <p class="subtitle">
            Silahkan Pilih Menu
          </p>
        </div>

        <br>

        <div class="columns">
		 <div class="column wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
            <div class="box">
              <div class="media">
                <div class="media-left">
                  <span class="icon">
                    <i class="fas fa-user is-size-4 has-text-dark"></i>
                  </span>
                </div>

                <div class="media-content">
                  <div class="content">
                    <p>
                      <strong>Pengguna</strong>
                    </p>
                    <p>
                      <?= ($this->session->login == TRUE ? 'Show Your Profile..' : 'Daftar/Login dulu..'); ?>
                    </p>
                    <a
                      class="button is-dark"
                      href="<?= ($this->session->login == TRUE ? base_url('u/') . $this->session->username : base_url('login')); ?>"
                    >
                      <span>Klik Disini</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="column wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.2s">
            <div class="box">
              <div class="media">
                <div class="media-left">
                  <span class="icon">
                    <i class="fas fa-crosshairs is-size-4 has-text-link"></i>
                  </span>
                </div>

                <div class="media-content">
                  <div class="content">
                    <p>
                      <strong>Lokasi</strong>
                    </p>
                    <p>
                      Posisi anda sekarang..
                    </p>
                    <a class="button is-link" href="<?= base_url('my'); ?>">
                      <span>Klik Disini</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>

		  <div class="column wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">
            <div class="box">
              <div class="media">
                <div class="media-left">
                  <span class="icon">
                    <i class="fas fa-pen is-size-4 has-text-success"></i>
                  </span>
                </div>

                <div class="media-content">
                  <div class="content">
                    <p>
                      <strong>Input Data dari Lokasi</strong>
                    </p>
                    <p>
                      Anda bisa memasukan lokasi fasilitas, prasarana dan sentra ekonomi
                    </p>
                    <a class="button is-success" href="<?= base_url('draw'); ?>">
                      <span>Klik Disini</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
		  
          <div class="column wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s">
            <div class="box">
              <div class="media">
                <div class="media-left">
                  <span class="icon">
                    <i class="fas fa-map-marker-alt is-size-4 has-text-danger"></i>
                  </span>
                </div>

                <div class="media-content">
                  <div class="content">
                    <p>
                      <strong>Input Data Peta Google</strong>
                    </p>
                    <p>
                      Anda bisa mengupdate informasi yang ada diPeta Google
                    </p>
                    <a class="button is-danger" href="<?= base_url('map') ?>">
                      <span>Klik Disini</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          

          <div class="column wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s">
            <div class="box">
              <div class="media">
                <div class="media-left">
                  <span class="icon">
                    <i class="fas fa-book is-size-4 has-text-primary"></i>
                  </span>
                </div>

                <div class="media-content">
                  <div class="content">
                    <p>
                      <strong>Kontributor</strong>
                    </p>
                    <p>
                      Daftarkan Diri..
                    </p>
                    <a class="button is-primary" href="<?= base_url('guest'); ?>">
                      <span>Silahkan Masuk</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>

         
        </div>
      </div>
    </div>
  </section>

  <footer class="footer">
    <div class="content">
      <div class="columns">
        <div class="column">
          <p class="subtitle">
            <strong>Survei dan Pemetaan</strong> Oleh <a href="http://dpupr.blorakab.go.id/">DPUPR Kab. Blora</a>
          </p>
          <p>
            Temukan dan Tempatkan Peta Potensi Kab. Blora
          </p>
          <div class="buttons">
            <a class="button is-dark" href="https://facebook.com/taryono.putranto">
              <span class="icon">
                <i class="fab fa-facebook"></i>
              </span>
            </a>

            <a class="button is-dark" href="https://instagram.com/andriannus">
              <span class="icon">
                <i class="fab fa-instagram"></i>
              </span>
            </a>

            <a class="button is-dark" href="https://twitter.com/andriannus">
              <span class="icon">
                <i class="fab fa-twitter"></i>
              </span>
            </a>
          </div>
        </div>

        <div class="column">
          <p class="subtitle">
            <strong>Source Code</strong> on Github
          </p>
          <p>
            <iframe
              src="https://ghbtns.com/github-btn.html?user=andriannus&repo=searchmap&type=star&count=true&size=large"
              frameborder="0" scrolling="0" width="160px" height="30px"
            ></iframe>
          </p>
          <p>Licensed <a href="https://github.com/andriannus/searchmap/blob/master/LICENSE">MIT</a></p>
        </div>

        <div class="column">
          <p class="subtitle">
            <strong>Terimakasih kpd</strong>
          </p>
          <div class="field is-grouped is-grouped-multiline">
            <div class="control">
              <div class="tags has-addons">
                <span class="tag is-dark">codeigniter</span>
                <span class="tag">3.1.9</span>
              </div>
            </div>

            <div class="control">
              <div class="tags has-addons">
                <span class="tag is-dark">vue.js</span>
                <span class="tag">2.5.17</span>
              </div>
            </div>

            <div class="control">
              <div class="tags has-addons">
                <span class="tag is-dark">lodash</span>
                <span class="tag">4.17.10</span>
              </div>
            </div>

            <div class="control">
              <div class="tags has-addons">
                <span class="tag is-dark">validate.js</span>
                <span class="tag">0.12.0</span>
              </div>
            </div>

            <div class="control">
              <div class="tags has-addons">
                <span class="tag is-dark">moment.js</span>
                <span class="tag">2.22.2</span>
              </div>
            </div>

            <div class="control">
              <div class="tags has-addons">
                <span class="tag is-dark">particles.js</span>
                <span class="tag">2.0.0</span>
              </div>
            </div>

            <div class="control">
              <div class="tags has-addons">
                <span class="tag is-dark">wow.js</span>
                <span class="tag">latest</span>
              </div>
            </div>

            <div class="control">
              <div class="tags has-addons">
                <span class="tag is-dark">bulma</span>
                <span class="tag">0.7.1</span>
              </div>
            </div>

            <div class="control">
              <div class="tags has-addons">
                <span class="tag is-dark">animate.css</span>
                <span class="tag">latest</span>
              </div>
            </div>

            <div class="control">
              <div class="tags has-addons">
                <span class="tag is-dark">google apis</span>
                <span class="tag">v3</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
</div>

<script>
particlesJS.load('particle-js', '<?= base_url() ?>' + 'assets/particles.json');

const index = new Vue({
  el: '#index',
  data: () => ({

  }),

  methods: {
  }
})
</script>
