<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div id="area">
	<div class="card" ref="mapCard" style="margin: 5px 0 0 5px" :style="{ display: visibleCard }">
		<header class="card-header">
			<p class="card-header-title">Menus</p>
		</header>

		<div class="card-content">
			<div class="content">
				<div class="field">
					<a href="<?= base_url('site') ?>" class="button is-large is-dark" title="Home">
						<span class="icon">
							<i class="fas fa-home"></i>
						</span>
					</a>
					<a href="<?= base_url('guest/area') ?>" class="button is-large is-info" title="Guest Book">
						<span class="icon">
							<i class="fas fa-book"></i>
						</span>
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="card" ref="mapCardDelete" style="margin: 5px 0 0 5px" :style="{ display: visibleCardDelete }">
		<header class="card-header">
			<p class="card-header-title">Rincian lokasi</p>
		</header>
		<div class="card-content">
			<div class="content">
				<div class="field" v-if="!saved">
					<button class="button is-large is-success" title="ketik" @click="switchModal">
						<span class="icon">
							<i class="fas fa-save"></i>
						</span>
					</button>
					<button class="button is-large is-danger" ref="buttonRemove" title="Delete Area">
						<span class="icon">
							<i class="fas fa-trash"></i>
						</span>
					</button>
				</div>

				<div class="field" v-if="saved">
					<button class="button is-success">
						<span class="icon">
							<i class="fas fa-check"></i>
						</span>
						<span>Saved</span>
					</button>
				</div>
			</div>
		</div>
		<footer class="card-footer">
			<a href="<?= base_url('site') ?>" class="card-footer-item">Home</a>
			<a href="<?= base_url('guest/area') ?>" class="card-footer-item has-text-centered">Lokasi Lokal</a>
		</footer>
	</div>

	<div id="map" ref="map" style="height: 100%; display: hidden"></div>

	<!-- Modal Save -->
	<div class="modal" :class="{ 'is-active': visibleModal }">
		<div class="modal-background"></div>
		<div class="modal-card">
			<header class="modal-card-head">
				<p class="modal-card-title">Guest Book</p>
				<button class="delete" aria-label="close" @click="switchModal"></button>
			</header>
			<section class="modal-card-body">

				<!-- mulai dari sini -->
		<div class="field">
          <label class="label">Pilih Kecamatan</label>
          <div class="control has-icons-left">
        <select id="kecamatan" v-model="selectedKecamatanvalue" @change="selectKecamatan">
				<option value="-1">--Pilih--</option>
				<option value="BLORA_KOTA">BLORA_KOTA</option>
				<option value="KUNDURAN">KUNDURAN</option>
				<option value="KRADENAN">KRADENAN</option>
				
			</select><br/><br/>
    
			<label class="label">Pilih Kelurahan : </label>
			<select id="kelurahan" name="kelurahan" v-model="selectedOption">
        <option value="-1">--Pilih--</option>
        <option v-for="option in kecamatanOption" :value="option.value" >{{ option.display }}</option>
			<!--Dependent Select option field-->
			</select><br/><br/>
          </div>
        </div>
		<!-- berhenti disini -->
		<!-- mulai dari sini -->
		<div class="field">
          <label class="label">Pilih Kategori Fasum :</label>
          <div class="control has-icons-left">
            <select id="country" v-model="selectedCountry" @change="selectCountry">
				<option value="-1">--Select--</option>
				<option>KESEHATAN</option>
				<option>PENDIDIKAN</option>
				<option>INDUSTRI</option>
				<option>PERIBADATAN</option>
				<option>PERTOKOAN</option>
				<option>PASAR</option>
				<option>PERKANTORAN</option>
				<option>RTH</option>
				<option>TRANSPORTASI</option>
			</select><br/><br/>
			<label class="label" name="prasarana">Pilih Fasilitas : </label>
			<select id="city"  v-model="selectedCountryOption">
      <option value="-1">--Select--</option>
        <option v-for="option in countryOption" :value="option.value" >{{ option.display }}</option>
			<!--Dependent Select option field-->
			</select><br/><br/>
      
          </div>
        </div>
		<!-- berhenti disini -->
		
				<div class="field">
					<label class="label">Name</label>
					<div class="control has-icons-left">
						<input class="input" type="text" placeholder="Name or Initial" v-model="name"></input>
						<span class="icon is-small is-left">
							<i class="fas fa-user"></i>
						</span>
					</div>
				</div>
				<div class="field">
					<label class="label">Area Name</label>
					<div class="control has-icons-left">
						<input class="input" type="text" placeholder="Area Name" v-model="areaName"></input>
						<span class="icon is-small is-left">
							<i class="fas fa-user"></i>
						</span>
					</div>
				</div>
				<div class="field">
					<label class="label">Area Image</label>
					<div class="control has-icons-left">
						<input class="input" type="file" placeholder="Area Image" v-on:change="upload($event)" ref="file" accept="image/*" ></input>
						<span class="icon is-small is-left">
							<i class="fas fa-user"></i>
						</span>
					</div>
				</div>
			</section>
			<footer class="modal-card-foot">
				<button class="button is-success" @click="saveArea" v-if="!inTheProcess" :disabled="!name || !areaName || uploading">Save</button>
				<button class="button is-success is-loading" v-if="inTheProcess">Save</button>
				<button class="button" @click="switchModal">Cancel</button>
			</footer>
		</div>
	</div>
</div>

<script>
/*
|--------------------------------------------------------------------------
| Vue.js
|--------------------------------------------------------------------------
|
| new Vue({}) -> Instance Vue.js
|
| Digunakan untuk mengawali Vue.js
|
| el 			-> Target yang akan dimanupulasi oleh Vue.js
| data 		-> Data (variabel) pada Vue.js
| methods	-> Menampung Method yang akan digunakan
|
| {{}}		-> Menampilkan data (variabel)
| @click	-> Melakukan method tertentu ketika bagian tersebut diklik
|
| Untuk lebih lengkapnya, silahkan kunjungi:
| https://vuejs.org
|
*/

const area = new Vue({
	el: '#area',
	data: () => ({
		map: '',
		selectedKecamatanvalue:"-1",
		selectedOption:"",
		kecamatanOption:[],
		countryOption:[],
		selectedCountry:"-1",
		selectedCountryOption : "",
		drawingManager: '',
		circle: '',
		rectangle: '',
		polyline: '',
		polygon: '',
		name: '',
		areaName: '',
		areaImage: '',
		areaLoc: '',
		areaType: '',
		newShape: '',
		visibleCard: 'none',
		visibleCardDelete: 'none',
		visibleModal: false,
		inTheProcess: false,
		saved: false,
		uploading: false,
        lat: -6.9697748,
        lng: 111.4144941,
        marker: '',
        latLng: ''
	}),

	mounted () { // Silahkan lihat LifeCycle Vue.js

        this.getLocation();
		this.initMap()
	},

	methods: {
		selectCountry:function() {
      this.selectedCountryOption = '';
      var country_array = {
      'KESEHATAN':[
          {display: "Rumah Sakit", value: "Rumah-Sakit" }, 
          {display: "Puskesmas", value: "Puskesmas" }, 
          {display: "Pustu", value: "Pustu" },
          {display: "Klinik", value: "Klinik" }]
      ,
      'PENDIDIKAN':[
          {display: "Sekolah Dasar", value: "SD" }, 
          {display: "Sekolah Menengah Pertama", value: "SLTP" }, 
          {display: "Sekolah Menengah Atas", value: "SLTA" },
          {display: "Universita", value: "Universitas" },
          {display: "Akademi ", value: "Akademi" }]
      ,
	  'PERIBADATAN':[
          {display: "Masjid", value: "Masjid" }, 
          {display: "Gereja", value: "Gereja" }, 
          {display: "Vihara", value: "Vihara" },
          {display: "Pamujan", value: "Pamujan" }]
      ,
	  'INDUSTRI':[
          {display: "Rumah Tangga", value: "Rumah_tangga" }, 
          {display: "Industri Kecil", value: "Indst_Kecil" }, 
          {display: "Industri Menengah", value: "Indst_Menengah" },
          {display: "Industri Besar", value: "Indst_Besar" }]
      ,
	  'PERTOKOAN':[
          {display: "Pertokoan Besar", value: "Pertokoan_Besar" }, 
          {display: "Pertokoan Kecil", value: "Pertokoan_Kecil" }, 
          {display: "Pertokoan Deret", value: "Pertooan_Deret" }]
      ,
	  'PASAR':[
          {display: "Pasar Modern", value: "Pasar_Modern" }, 
          {display: "Pasar Rakyat", value: "Pasar_Rakyat" }]
      ,
	  'PERKANTORAN':[
          {display: "Perkantoran Pemerintah", value: "Perkantoran_Pemerintah" }, 
          {display: "Perkantoran Jasa", value: "Perkantoran_Jasa" }]
      ,
	  'RTH':[
          {display: "Lap Olah Raga", value: "Lap_Olah_Raga" }, 
          {display: "Hutan Kota", value: "Hutan_Kota" }, 
          {display: "Jalur Hijau", value: "Jalur_Hijau" },
          {display: "Taman Kota", value: "Taman_kota" },
          {display: "Makam ", value: "Makam" }]
      ,
      "TRANSPORTASI":[
        {display: "Stasiun", value: "Stasiun" }, 
        {display: "Terminal", value: "Terminal" }]
      
      };
      this.countryOption = country_array[this.selectedCountry];
     
      //this.selectedDrinkLabel = this.drinks[this.selectedDrink].label;
    },
    selectKecamatan:function() {
      this.selectedOption = '';
      var kecamatan_array = {
      'BLORA_KOTA':[
          {display: "Tempelan", value: "Tempelan" }, 
          {display: "Kunden", value: "Kunden" },
          {display: "Kauman", value: "Kauman" },
          {display: "Sonorejo", value: "Sonorejo" },
		  {display: "Tambahrejo", value: "Tambahrejo" },
		  {display: "Mlangsen", value: "Mlangsen" },
		  {display: "Beran", value: "Beran" },
		  {display: "Kedung Jenar", value: "KedungJenar" },
		  {display: "Bangkle", value: "Bangkle" },
		  {display: "Andongrejo", value: "Andongrejo" },
		  {display: "Jejeruk", value: "Jejeruk" },
		  {display: "Jepangrejo", value: "Jepangrejo" },
		  {display: "Kamolan", value: "Kamolan" },
		  {display: "Karangjati", value: "Karangjati" },
		  {display: "Ngadipurwo", value: "Ngadipurwo" },
		  {display: "Ngampel", value: "Ngampel" },
		  {display: "Patalan", value: "Patalan" },
		  {display: "Pelem", value: "Pelem" },
		  {display: "Plantungan", value: "Plantungan" },
		  {display: "Purworejo", value: "Purworejo" },
		  {display: "Purwosari", value: "Purwosari" },
		  {display: "Sendangharjo", value: "Sendangharjo" },
		  {display: "Tambaksari", value: "Tambaksari" },
		  {display: "Tegalgunung", value: "Tegalgunung" },
		  {display: "Tempuran", value: "Tempuran" },
		  {display: "Tempurejo", value: "Tempurejo" },
		  {display: "Temurejo", value: "Temurejo" },
          {display: "Jetis", value: "Jetis" }]
      ,
      'KUNDURAN':[
        {display: "Bakah", value: "Bakah" },
        {display: "Balong ", value: "Balong" }]
      ,
      "KRADENAN":[
            {display: "Getas", value: "Getas" },
            {display: "Medalem ", value: "Medalem" }]
      
      };
      this.kecamatanOption = kecamatan_array[this.selectedKecamatanvalue];
     
      //this.selectedDrinkLabel = this.drinks[this.selectedDrink].label;
    },
		initMap () {
			// 'area' -> 'const app = new Vue({})'

            this.latLng = new google.maps.LatLng(this.lat, this.lng);
			// Menyimpan data map pada Vue.js
			this.map = new google.maps.Map(this.$refs.map, { // atribut 'ref' dengan value 'map'
				center: {
					lat: this.lat,
					lng: this.lng
				},

				zoom: 19.75,
				disableDefaultUI: true // Menghilangkan tombol - tombol default Google Maps
			});

            this.marker = new google.maps.Marker({
                map: this.map,
                position: this.latLng,
                anchorPoint: new google.maps.Point(0, -29)
            });

			const drawingModeOptions = {
				editable: false
			}

			this.drawingManager = new google.maps.drawing.DrawingManager({
				drawingMode: google.maps.drawing.OverlayType.POLYLINE,
				drawingControlOptions: {
					// Shape apa saja yang dapat dibuat
					drawingModes: ['polyline', 'rectangle', 'circle', 'polygon',]
				},
				polylineOptions: drawingModeOptions,
				rectangleOptions: drawingModeOptions,
				circleOptions: drawingModeOptions,
				polygonOptions: drawingModeOptions,
				map: this.map
			})

			// Ketika shape (apapun) selesai dibuat
			google.maps.event.addListener(this.drawingManager, 'overlaycomplete', function (shape) {
				area.drawingManager.setMap(null),
				area.newShape = shape.overlay,
				area.visibleCard = 'none',
				area.visibleCardDelete = 'block'
			})

			// Ketika polyline selesai dibuat
			google.maps.event.addListener(this.drawingManager, 'polylinecomplete', function (polyline) {
				let arrayPolyline = []
				let data = polyline.getPath().getArray()

				for (let i=0; i<data.length; i++) {
					arrayPolyline.push(data[i].lat() + ', ' + data[i].lng())
				}

				area.areaType = 'polyline'
				area.areaLoc = JSON.stringify(arrayPolyline)
			})

			// Ketika rectangle selesai dibuat
			google.maps.event.addListener(this.drawingManager, 'rectanglecomplete', function (rectangle) {
				let jsonRectangle = {
					west: rectangle.bounds.b.b,
					east: rectangle.bounds.b.f,
					south: rectangle.bounds.f.b,
					north: rectangle.bounds.f.f
				}

				area.areaType = 'rectangle'
				area.areaLoc = JSON.stringify(jsonRectangle) // Mengubah Array/Object menjadi sebuah String
			})

			// Ketika circle selesai dibuat
			google.maps.event.addListener(this.drawingManager, 'circlecomplete', function (circle) {
				let jsonCircle = {
					center: {
						lat: circle.center.lat(),
						lng: circle.center.lng(),
					},
					radius: circle.radius
				}

				area.areaType = 'circle'
				area.areaLoc = JSON.stringify(jsonCircle) // Mengubah Array/Object menjadi sebuah String
			})

			// Ketika polygon selesai dibuat
			google.maps.event.addListener(this.drawingManager, 'polygoncomplete', function (polygon) {
				let arrayPolygon = []
				let data = polygon.getPath().getArray()

				for (let i=0; i<data.length; i++) {
					arrayPolygon.push(data[i].lat() + ', ' + data[i].lng())
				}

				area.areaType = 'polygon'
				area.areaLoc = JSON.stringify(arrayPolygon) // Mengubah Array/Object menjadi sebuah String
			})

			// Silahkan lihat tag HTML dengan atribut 'ref' = 'buttonRemove'
			// Ketika tag tersebut diklik
			google.maps.event.addDomListener(this.$refs.buttonRemove, 'click', function () {
				area.drawingManager.setMap(area.map),
				area.newShape.setMap(null),
				area.visibleCard = 'block',
				area.visibleCardDelete = 'none'
			})

			let card = this.$refs.mapCard // Silahkan lihat tag HTML dengan atribut 'ref' = 'mapCard'
			let cardDelete = this.$refs.mapCardDelete
			let button = this.$refs.buttonSave

			// Menampilkan card atau cardDelete pada Map
			this.map.controls[google.maps.ControlPosition.LEFT_TOP].push(card);
			this.map.controls[google.maps.ControlPosition.LEFT_TOP].push(cardDelete);

			// Ketika Maps selesai dimuat
			google.maps.event.addListenerOnce(this.map, 'tilesloaded', function () {
				area.visibleCard = 'block'
			})


		},

		// Menyimpan data area
		saveArea () {
			this.inTheProcess = true
			let formData = new FormData();
			formData.append('name', this.name);
			formData.append('area_image', this.areaImage);
			formData.append('area_name', this.areaName);
			formData.append('area_type', this.areaType);
			formData.append('area', this.areaLoc);
			formData.append('kecamatan', this.selectedKecamatanvalue);
			formData.append('kelurahan', this.selectedOption);
			formData.append('country', this.selectedCountry);
			formData.append('city', this.selectedCountryOption);

			let data = 	'name=' + this.name +
									'&area_name=' + this.areaName +
									'&area_type=' + this.areaType +
									'&area=' + this.areaLoc +
									'&kecamatan=' + this.selectedKecamatanvalue +
									'&kelurahan=' + this.selectedOption +
									'&country=' + this.selectedCountry +
									'&city=' + this.selectedCountryOption

			// Axios post (sama seperti jQuery AJAX)
			axios.post('<?= base_url() ?>' + 'api/storeArea', formData,
				{
					headers: {
						'Content-Type': 'multipart/form-data'
					}
				})
				// then / catch -> Promise JavaScript
				.then(res => {
					this.inTheProcess = false
					this.visibleModal = false
					console.log(res)
					this.saved = true
				})
				.catch(err => {
					console.log(err)
				})
		},
        getLocation() {
            navigator.geolocation.getCurrentPosition(this.showLocation, this.showError);
        },

        showLocation(position) {
            this.error = false;
            this.lat = position.coords.latitude;
            this.lng = position.coords.longitude;

            this.initMap();
        },

        showError(err) {
            this.error = true;

            switch (err.code) {
                case 1: // Permission Denied
                    this.errorMsg = err.message
                    break;

                case 2: // Position Unavailable
                    this.errorMsg = err.message
                    break;

                case 3: // Timeout
                    this.errorMsg = err.message
                    break;
            }
        },

		switchModal () {
			this.name = ''
			this.visibleModal = !this.visibleModal
		},
		upload(event) {
			this.areaImage = this.$refs.file.files[0];
        }
	}
})
</script>
