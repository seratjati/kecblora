<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Api Controller
|--------------------------------------------------------------------------
|
| Digunakan untuk menampilkan data menggunakan AJAX
|
*/

class Api extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }


        // Load Model
		$this->load->model('m_guest', 'guest');
	}

	// Menampilkan semua data tempat
	public function getAllPlaces()
	{
		$places = $this->guest->getAllPlaces()->result();

		// Menampilkan output dalam bentuk JSON
		return $this->output
								->set_status_header(200) // Respon HTTP Success
								->set_output(json_encode([
										'success' => true,
										'message' => 'Success get all recommend places',
										'data' => $places
									]));
	}

	// Menampilkan semua data area
	public function getAllAreas()
	{
		$area = $this->guest->getAllAreas()->result();

		// Menampilkan output dalam bentuk JSON
		return $this->output
								->set_status_header(200)
								->set_output(json_encode([
										'success' => true,
										'message' => 'Success get all recommend area',
										'data' => $area
									]));
	}

	public function getAllPlacesByUsername($username)
	{
		$places = $this->guest->getAllPlacesByUsername($username)->result();

		return $this->output
								->set_status_header(200) // Respon HTTP Success
								->set_output(json_encode([
										'success' => true,
										'message' => 'Success get all recommend places by username',
										'data' => $places
									]));
	}

	// Menampilkan satu data tempat
	public function getOnePlace($id)
	{
		$place = $this->guest->getOnePlace($id)->row();

		if ($place === null) {
			// Menampilkan output dalam bentuk JSON
			return $this->output
									->set_status_header(404) // Status HTTP Not Found
									->set_output(json_encode([
											'success' => false,
											'message' => 'Not found'
										]));

		} else {
			// Menampilkan output dalam bentuk JSON
			return $this->output
									->set_status_header(200)
									->set_output(json_encode([
											'success' => true,
											'message' => 'Success get one place',
											'data' => $place
										]));
		}
	}

	// Menampilkan satu data area
	public function getOneArea($id)
	{
		$area = $this->guest->getOneArea($id)->row();

		if ($area === null) {
			// Menampilkan output dalam bentuk JSON
			return $this->output
									->set_status_header(404)
									->set_output(json_encode([
											'success' => false,
											'message' => 'Not found'
										]));

		} else {
			// Menampilkan output dalam bentuk JSON
			return $this->output
									->set_status_header(200)
									->set_output(json_encode([
											'success' => true,
											'message' => 'Success get one area',
											'data' => $area
										]));
		}
	}

	// Simpan tempat
	public function storePlace()
	{
		$id_user = $this->input->post('id_user');
		$place = $this->input->post('place');
		$address = $this->input->post('address');
		$lat = $this->input->post('lat');
		$lng = $this->input->post('lng');

		$data = [
			'id_user' => $id_user,
			'place' => $place,
			'address' => $address,
			'lat' => $lat,
			'lng' => $lng
		];

		$result = $this->guest->storePlace($data);

		if (!$result) {
			// Menampilkan output dalam bentuk JSON
			return $this->output
									->set_status_header(500) // Status HTTP Error
									->set_output(json_encode([
											'success' => false,
											'message' => 'Error'
										]));

		} else {
			// Menampilkan output dalam bentuk JSON
			return $this->output
									->set_status_header(201)
									->set_output(json_encode([
											'success' => true,
											'message' => 'Success save place'
										]));
		}
	}

	// Simpan area
	public function storeArea()
	{
		$name = $_POST['name'];
		$area_name = $_POST['area_name'];
		$area_type =  $_POST['area_type'];
		$area = $_POST['area'];
		$kecamatan = $_POST['kecamatan'];
		$kelurahan	 = $_POST['kelurahan'];
		$country = $_POST['country'];
		$city = $_POST['city'];
		$area_image = null;

		

		// Upload image
		if (isset($_FILES["area_image"])) {
			$target_dir = "assets/imagelocation/";
			$area_image = time().'_' . basename($_FILES["area_image"]["name"]);
			$target_file = $target_dir.$area_image;
			$uploadOk = 1;
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			$check = getimagesize($_FILES["area_image"]["tmp_name"]);

			if($this->is_image($imageFileType) && $check !== false) {
				if (!move_uploaded_file($_FILES["area_image"]["tmp_name"], $target_file)) {
					$area_image = null;
				}
			}
		}

		$data = [
			'name' => $name,
			'area_name' => $area_name,
			'area_type' => $area_type,
			'area' => $area,
			'area_image' => $area_image,
			'kecamatan' => $kecamatan,
			'kelurahan' => $kelurahan,
			'country' => $country,
			'city' => $city
		];

		$result = $this->guest->storeArea($data);

		if (!$result) {
			// Menampilkan output dalam bentuk JSON
			return $this->output
									->set_status_header(500)
									->set_output(json_encode([
											'success' => false,
											'message' => 'Error'
										]));

		} else {
			// Menampilkan output dalam bentuk JSON
			return $this->output
									->set_status_header(201) // Status HTTP Success Created
									->set_output(json_encode([
											'success' => true,
											'message' => 'Success save place'
										]));
		}
	}

	public function is_image($image_type)
	{
		if(in_array($image_type , array("jpg", "png", "jpeg", "gif")))
		{
			return true;
		}

		return false;
	}

	// Menghapus tempat
	public function destroyPlace($id)
	{
		$result = $this->guest->deletePlace($id);

		if (!$result) {
			return $this->output
									->set_status_header(500)
									->set_output(json_encode([
											'success' => false,
											'message' => 'Error'
										]));

		} else {
			return $this->output
									->set_status_header(201)
									->set_output(json_encode([
											'success' => true,
											'message' => 'Success delete place'
										]));
		}
	}

	// Menghapus area
	public function destroyArea($id)
	{
		$result = $this->guest->deleteArea($id);

		if (!$result) {
			return $this->output
									->set_status_header(500)
									->set_output(json_encode([
											'success' => false,
											'message' => 'Error'
										]));

		} else {
			return $this->output
									->set_status_header(201)
									->set_output(json_encode([
											'success' => true,
											'message' => 'Success delete area'
										]));
		}
	}
}
